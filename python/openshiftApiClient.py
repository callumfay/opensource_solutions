import requests
import json
########################################
#
# First stab at using the Oshift API in anger
#
########################################
def find_between( resp, first, last ):
    try:
        start = resp.index( first ) + len( first )
        end = resp.index( last, start )
        return resp[start:end]
    except ValueError:
        return ""

class openshiftApiClient:

  baseuri = None
  def __init__(self, baseuri):
    self.baseuri = baseuri

  def getApiToken(self, user):
    self.baseuri
    headers = {'Content-Type':'application/json','Authorization':"Basic " + user + ""}
    resp = requests.get(baseuri + '/oauth/authorize?response_type=code&client_id=openshift-challenging-client', allow_redirects=False, verify=False, headers=headers)
    token = urlparse.urlparse(resp.headers["location"]).query
    return urlparse.parse_qs(token)["code"][0]

  def createNamespace(self, name, token):
    self.baseuri
    baseuri = 'https://vrhbpipsu202:8443'
    headers = {'Content-Type':'application/json','Authorization':"Bearer " + token + ""}
    namespace = {"metadata":
                  {"name":name
                  }
                }
    create = request.post(baseuri + "/oapi/v1/projects", data=json.dumps(namespace),  headers=headers, verify=False)
    if create.return_code != 200
      print create.content
      return -1
    return create.content

  def postDeployConf(self, filename, token):
    self.baseuri
    headers = {'Content-Type':'application/json','Authorization':"Bearer " + token + ""}
    compdata = open(filename, 'r')
    compdata = json.load(compdata)["deploymentconfig"]
    namespace = compdata["namespace"][0]
    create = request.post(baseuri + "/oapi/v1/namespaces/" + namespace + "/deploymentconfigs" , data=compdata,  headers=headers, verify=False)
    if create.return_code != 200
      print create.content
      return -1
    return create.content
