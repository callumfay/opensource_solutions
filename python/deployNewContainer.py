import sys
import os
import json
import subprocess
import shutil
import collections
import socket
from jinja2 import Environment


# Retrieve the config instance from chieftain and take require variables

config_id = os.getenv("CONFIG_ID", "none")
config = retreiveInstance(config_id)
name = config["name"]
service = config["openshift_service"]
password = config["nexus_password"]
namespace = config["namespace"]
adminuser = config["nexus_user"]
docker_url = config["docker_url"]
pull_url = config["pull_url"]
o_shift_user = config["openshift_user"]
o_shift_pass = config["openshift_password"]
docker_reg = config["registry_url"]
imagestream = docker_reg + "/" + namespace + "/" + name

##
print "Here is the instanced config"
debugprint(config)
##

# Login to openshift with service account

subprocess.call([
    "oc",
    "login",
    "-u", o_shift_user,
    "-p", o_shift_pass
    ])


# Copy down the nexus image from our external docker repo

print "Pulling down image from our external nexus"

print "Docker commands now running"

print "##################################################"

print [
  "docker",
  "pull", pull_url
  ]

print "##################################################"

print [
  "docker",
  "tag",  pull_url , imagestream
  ]

print "##################################################"

print [
  "docker",
  "push", imagestream
  ]

##############################################
# Comands run section below
##############################################

subprocess.call([
  "docker",
  "pull", pull_url
  ])

subprocess.call([
  "docker",
  "tag",  pull_url , imagestream
  ])

##############################################
#  Log into docker for image push and pull
##############################################
subprocess.call([
  "docker",
  "login",
  "-u", subprocess.check_output([ "oc", "whoami" ]),
  "-p", subprocess.check_output([ "oc", "whoami", "-t" ]),
  "-e", "null@cybg.com", docker_reg
  ])

print [
  "docker",
  "login",
  "-u", subprocess.check_output([ "oc", "whoami" ]),
  "-p", subprocess.check_output([ "oc", "whoami", "-t" ]),
  "-e", "default@cybg.com", docker_reg
  ]
##############################################

subprocess.call([
  "docker",
  "push", imagestream
  ])

# Now we run the oc commands to create the application

print "Below are the commands we are going to run to create this application"

print "##################################################"

print [
    "oc",
    "new-app",
    "-f", "temp.bak.json",
    "--namespace=" + namespace,
    "--insecure-registry",
    "--insecure-registry=true",
    "-p", "NAME=" + name,
    "-p", "USER=" + adminuser,
    "-p", "PASSWORD=" + password,
    "-p", "SERVICE=" + service,
    "-p", "IMAGESTREAM=" + imagestream
    ]

##############################################
# Comands run section below
##############################################

subprocess.call([
    "oc",
    "new-app",
    "-f", "temp.bak.json",
    "--namespace=" + namespace,
    "--insecure-registry",
    "--insecure-registry=true",
    "-p", "NAME=" + name,
    "-p", "USER=" + adminuser,
    "-p", "PASSWORD=" + password,
    "-p", "SERVICE=" + service,
    "-p", "IMAGESTREAM=" + imagestream
    ])

# Now we can check to see if the service is to be exposed for this container

print "Checking to see if the service component is to be exposed"

if ("expose_service" in config == "true") or (config["expose_service"] == "true"):
  subprocess.call([
    "oc",
    "expose", "service",
    service,
    "-n",
    namespace
    ])
else:
  print "Service has not to be exposed"