#!/bin/bash
#
# Callum Fay 09/11/2016
#
#################################
#
#   Preset variables below
#
#################################
build=vm_build.json
tmpfile=post.json
source=contruct.json
python -m pythonworkbench.getComponentConfig > $source

#
## Extract source JSON
#

user=$( jq -r '.cf_service_acct' $source )
password=$( jq -r '.cf_service_pword' $source )
cforms=$( jq -r '.cloudforms_url' $source )
sat_url=$( jq -r '.full_sat_url' $source )
request=$( jq '.request' $source)
href=$( jq -r '.request.resource.href' $source )

####################### MAIN CODE SECTION ########################
# Get information from the config manager -
    
  if [[ -s $source ]]; then
    echo "$source has config data"
    else
    echo "$source has no config data, aborting."
    exit 1
  fi
  echo "Collecting information from the config manager"
  echo $request
  echo "Moving request into a build file for use later"
  echo $request > $build
  
# Query InfoBlox for new IP and Reserve it -

  echo "Acquiring and reserving IP Address"
  name=$( jq '.resource.vm_name' $build )
  echo "Sending build file to Cloudforms API...  $href"
  curl --insecure -H "Content-Type: application/json" -X POST -u $user:$password --data "@$build" $href | python -mjson.tool
  if [ ! $? = 0 ]; then
    echo "Issue sending data to cloudforms please review the build json:"
    cat $build
    exit 1
  fi
  
# Check request status -

  echo "Checking status of build, this script will error if there is an issue encountered. Please wait approx 15 minutes..."
  state=$(curl -k -s "https://$user:$password@$cforms/api/v2.2.0/service_requests?expand=resources&sort_order=asc" | python -mjson.tool | egrep -i "$name" -A10 | grep -i request_state | tail -1 | awk '{print $2}' | sed 's/"//g' | cut -d, -f1)
  echo "Current request state: $state"
  until [[ $state = "finished" ]];
    do
    sleep 120
    state=$(curl -k -s "https://$user:$password@$cforms/api/v2.2.0/service_requests?expand=resources&sort_order=asc" | python -mjson.tool | egrep -i "$name" -A10 | grep -i request_state | tail -1 | awk '{print $2}' | sed 's/"//g' | cut -d, -f1)
    if [ ! $? = 0 ]; then
      echo "Request has failed, please review cloudforms logs for more information"
      exit 1
    fi
    echo -ne "$state"
  done
  
# Check request return value -

  echo "Request complete checking status.."
  state1=$(curl -k -s "https://$user:$password@$cforms/api/v2.2.0/service_requests?expand=resources&attributes=status,options&sort_by=id&sort_order=asc" | python -mjson.tool | egrep -i -A10 "$name"  | grep -i status | tail -1 | awk '{print $2}' | sed 's/"//g')
  if [ $state1 = "Ok" ]; then
    echo "Status is $state1, build complete"
    elif [ ! $state1 = "Ok" ]; then
    echo "Status is $state1, build encountered an issue. Please check the logs from cloudforms for more information"
    exit 1
  fi
  
# Check status of the build in satellite -

  echo "Checking the satellite to see when the configuration has been applied and we can then run the next part in the pipeline.."
  state2=$(curl -k -s  "$sat_url/api/v2/hosts?search=$name" | python -m json.tool | grep -i configuration_status_label | awk '{print $2}' | sed 's/"//g' | cut -d, -f1)
  until [[ $state2 = "Active" ]];
    do
    sleep 120
    state2=$(curl -k -s  "$sat_url/api/v2/hosts?search=$name" | python -m json.tool | grep -i configuration_status_label | awk '{print $2}' | sed 's/"//g' | cut -d, -f1)
    if [ ! $? = 0 ]; then
      echo $?
      echo "Build has encountered an issue please investigate on the satellite server, hostname = $name"
      exit 1
    fi
    echo -ne "$state2"
  done
  echo "Build complete, moving onto the next step in the pipeline"