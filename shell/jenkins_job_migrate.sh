#!/bin/bash

curl -s -k -u admin:password http://jenkins1.something.com/api/json/jobs | python -m json.tool > jenkins-jobs
jq '.jobs' jenkins-jobs | grep name | awk '{print $2}' | sed 's/\"//g' | cut -d, -f1 > jenkins2
for i in $(cat jenkins2)
  do
    curl -s -k -u admin:password http://jenkins1.something.com/job/$i/config.xml > configs/$i.xml
done

for i in $(cat jenkins2)
  do
    curl -s -k -u admin:password -X POST http://jenkins2.something.com/createItem?name=$i --data-binary @configs/$i.xml -H "Content-Type:text/xml"
done